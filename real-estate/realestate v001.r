###
###
### KAGGLE
### Leaf Classification
###
### Author: Andrew Szwec
### Date:   15/10/2016
###
### PLAN
### 1. Load data
### 2. Explore data
### 3. Prepare data
### 4. Train model
### 5. Test model performance using MAE (Mean Absolute Error)
### 6. Repeat
### 
### 


### 
### IMPORTS
### 
require(caret)
require(rpart)
#require(rpart.plot)             # Enhanced tree plots
#require(RColorBrewer)               # Color selection for fancy tree plot
require(rattle)
require(Hmisc)
require(randomForest)

### 
### FUNCTIONs
### 
## evaluation metric
evaluation <- function (pred, obs){
  RMSE <- sqrt(mean(  (log(obs)-log(pred))^2 , na.rm=TRUE))
  
  return( RMSE )
}


### 
### LOAD DATA
### 
setwd("C:/Users/ahtet/Sandbox/deloitte-kaggle/real-estate")

df <- read.csv('train.csv')
sub <- read.csv('test.csv')

########################################################
## DATA PREP
########################################################

xx <- subset(df, select=c('MSSubClass','LotFrontage', 'LotArea', 'OverallQual','OverallCond', 'MasVnrArea', 'BsmtFinSF1'
                          ,'BsmtFinSF2', 'BsmtUnfSF','TotalBsmtSF','X1stFlrSF','X2ndFlrSF'
                          ,'GrLivArea','BsmtFullBath','BsmtHalfBath','FullBath','HalfBath','BedroomAbvGr'
                          ,'KitchenAbvGr','TotRmsAbvGrd','Fireplaces','GarageCars','GarageArea'
                          ,'SalePrice'
                          ))

xx <- xx[complete.cases(xx), ]

# v2
# xx <- subset(df, select=-c(PoolQC,Fence, MiscFeature,Alley))
# xx <- xx[complete.cases(xx), ]


sapply(xx,class)

########################################################
## SPLIT INTO TRAIN TEST AND VALIDATION
########################################################
# Split the data into a model training and test set used to measure the performance of the algorithm
set.seed(556677)
inTrain     = createDataPartition(xx$SalePrice, p = 0.6)[[1]]
training    = xx[ inTrain,]      # 60% of records
temp        = xx[-inTrain,]      # 40% of reocrds

inTemp      = createDataPartition(temp$SalePrice, p = 0.5)[[1]]
validation  = temp[ inTemp,]      # 20% of records
testing     = temp[-inTemp,]      # 20% of reocrds

########################################################
## GLM
########################################################


set.seed(272738389)
gl <- glm(SalePrice ~ .-GrLivArea-TotalBsmtSF, data=training  )
summary(gl)

pred <- predict(gl, newdata = testing)
obs <- testing$SalePrice

evaluation(pred, obs)

########################################################
## random forest
########################################################

rf.model <- randomForest(SalePrice ~ .-GrLivArea-TotalBsmtSF, data = training, 
                         importance=T, proximity=T, localImp=T )

pred <- predict(rf.model, newdata = testing)
obs <- testing$SalePrice

evaluation(pred, obs)



########################################################
## random forest 2
########################################################

rf.model <- randomForest(SalePrice ~ ., data = training, 
                         importance=T, proximity=T, localImp=T)

pred <- predict(rf.model, newdata = testing)
obs <- testing$SalePrice

evaluation(pred, obs)




########################################################
## SCORE DATA
########################################################

s2 <- subset(sub, select=c('MSSubClass','LotFrontage', 'LotArea', 'OverallQual','OverallCond', 'MasVnrArea', 'BsmtFinSF1'
                          ,'BsmtFinSF2', 'BsmtUnfSF','TotalBsmtSF','X1stFlrSF','X2ndFlrSF'
                          ,'GrLivArea','BsmtFullBath','BsmtHalfBath','FullBath','HalfBath','BedroomAbvGr'
                          ,'KitchenAbvGr','TotRmsAbvGrd','Fireplaces','GarageCars','GarageArea'
                         
))

cm <- colMeans(s2,na.rm = TRUE )

s2$LotFrontage <- ifelse(is.na(s2$LotFrontage), 6.858036e+01,s2$LotFrontage )
s2$MasVnrArea <- ifelse(is.na(s2$MasVnrArea),  cm["MasVnrArea"] ,s2$MasVnrArea )
s2$BsmtFinSF1 <- ifelse(is.na(s2$BsmtFinSF1), cm["BsmtFinSF1"],s2$BsmtFinSF1 )
s2$BsmtFinSF2 <- ifelse(is.na(s2$BsmtFinSF2), cm["BsmtFinSF2"],s2$BsmtFinSF2 )
s2$BsmtUnfSF <- ifelse(is.na(s2$BsmtUnfSF),       cm["BsmtUnfSF"] ,s2$BsmtUnfSF    )
s2$TotalBsmtSF <- ifelse(is.na(s2$TotalBsmtSF),   cm["TotalBsmtSF"] ,s2$TotalBsmtSF   )
s2$BsmtFullBath <- ifelse(is.na(s2$BsmtFullBath), cm["BsmtFullBath"] ,s2$BsmtFullBath  )
s2$BsmtHalfBath <- ifelse(is.na(s2$BsmtHalfBath), cm["BsmtHalfBath"] ,s2$BsmtHalfBath   )
s2$GarageCars <- ifelse(is.na(s2$GarageCars),     cm["GarageCars"] ,s2$GarageCars     )
s2$GarageArea <- ifelse(is.na(s2$GarageArea),     cm["GarageArea"] ,s2$GarageArea    )




results <- predict(rf.model, newdata = s2)

submission <- cbind(Id=sub$Id, SalePrice=results)

write.csv(submission, file='ourResults.csv', row.names = F)





