###
###
### KAGGLE
### Leaf Classification
###
### Author: Andrew Szwec
### Date:   15/10/2016
###
### PLAN
### 1. Load data
### 2. Explore data
### 3. Prepare data
### 4. Train model
### 5. Test model performance using RMSE (Root mean square error)
### 6. Repeat
### 
### 

# Install this!
#install.packages("https://h2o-release.s3.amazonaws.com/h2o-ensemble/R/h2oEnsemble_0.1.8.tar.gz", repos = NULL)
#install.packages("RANN")
#install.packages("caret")

### 
### IMPORTS
### 
require(caret)
require(RANN)
library(h2oEnsemble)

### 
### FUNCTIONs
### 
## evaluation metric
RMSE <- function (pred, obs){
  RMSE <- sqrt(mean(  (log(obs)-log(pred))^2 , na.rm=TRUE))
  
  return( RMSE )
}


### 
### LOAD DATA
### 
setwd("C:/Users/ahtet/Sandbox/deloitte-kaggle/real-estate")

df <- read.csv('train.csv')
sub <- read.csv('test.csv')

########################################################
## DATA PREP
########################################################

df$YearBuilt <- as.numeric(df$YearBuilt)
df$GarageYrBlt <- as.numeric(df$GarageYrBlt)
df$YrSold <- as.numeric(df$YrSold)
df$YearRemodAdd <- as.numeric(df$YearRemodAdd)

# Get all the numeric cols
xx <- subset(df, select=c('MSSubClass','LotFrontage', 'LotArea', 'OverallQual','OverallCond', 'MasVnrArea', 'BsmtFinSF1'
                          ,'BsmtFinSF2', 'BsmtUnfSF','TotalBsmtSF','X1stFlrSF','X2ndFlrSF'
                          ,'GrLivArea','BsmtFullBath','BsmtHalfBath','FullBath','HalfBath','BedroomAbvGr'
                          ,'KitchenAbvGr','TotRmsAbvGrd','Fireplaces','GarageCars','GarageArea'
                          ,"GarageYrBlt", "YrSold", "YearBuilt",    "YearRemodAdd" , "MiscVal", "MoSold", "X3SsnPorch", "ScreenPorch", "PoolArea"
                          ,"WoodDeckSF","OpenPorchSF","EnclosedPorch", "LowQualFinSF"
                          ))

xx.p <- preProcess(xx, method = c('scale', 'center','knnImpute'))  # Impute missing values
yy <- predict(xx.p, newdata=xx)
yy <- data.frame(yy, SalePrice=df$SalePrice)
# sapply(yy, class)

########################################################
## Prepare the submission data
########################################################
sub$YearBuilt <- as.numeric(sub$YearBuilt)
sub$GarageYrBlt <- as.numeric(sub$GarageYrBlt)
sub$YrSold <- as.numeric(sub$YrSold)
sub$YearRemodAdd <- as.numeric(sub$YearRemodAdd)

zz <- subset(sub, select=c('MSSubClass','LotFrontage', 'LotArea', 'OverallQual','OverallCond', 'MasVnrArea', 'BsmtFinSF1'
                           ,'BsmtFinSF2', 'BsmtUnfSF','TotalBsmtSF','X1stFlrSF','X2ndFlrSF'
                           ,'GrLivArea','BsmtFullBath','BsmtHalfBath','FullBath','HalfBath','BedroomAbvGr'
                           ,'KitchenAbvGr','TotRmsAbvGrd','Fireplaces','GarageCars','GarageArea'
                           ,"GarageYrBlt", "YrSold", "YearBuilt",    "YearRemodAdd" , "MiscVal", "MoSold", "X3SsnPorch", "ScreenPorch", "PoolArea"
                           ,"WoodDeckSF","OpenPorchSF","EnclosedPorch", "LowQualFinSF"
))


zz.p <- preProcess(zz, method = c('scale', 'center','knnImpute')) ## NZV and Yeo Johnson Transform not helpful
ss <- predict(zz.p, newdata=zz)


########################################################
## START H2O
########################################################
h2o.init(nthreads = -1)

# import data frame
df.hex <- as.h2o(yy)

# Split data frame
df.split = h2o.splitFrame(data = df.hex, ratios = 0.95)
df.train= df.split[[1]]
df.test = df.split[[2]]


## INITIALISE SOME LEARNERS
h2o.glm.1 <- function(..., alpha = 0.0) h2o.glm.wrapper(..., alpha = alpha)
h2o.glm.2 <- function(..., alpha = 0.5) h2o.glm.wrapper(..., alpha = alpha)
h2o.glm.3 <- function(..., alpha = 1.0) h2o.glm.wrapper(..., alpha = alpha)
h2o.randomForest.1 <- function(..., ntrees = 200, nbins = 50, seed = 1) h2o.randomForest.wrapper(..., ntrees = ntrees, nbins = nbins, seed = seed)
h2o.randomForest.2 <- function(..., ntrees = 200, sample_rate = 0.75, seed = 1) h2o.randomForest.wrapper(..., ntrees = ntrees, sample_rate = sample_rate, seed = seed)
h2o.randomForest.3 <- function(..., ntrees = 200, sample_rate = 0.85, seed = 1) h2o.randomForest.wrapper(..., ntrees = ntrees, sample_rate = sample_rate, seed = seed)
h2o.randomForest.4 <- function(..., ntrees = 200, nbins = 50, balance_classes = TRUE, seed = 1) h2o.randomForest.wrapper(..., ntrees = ntrees, nbins = nbins, balance_classes = balance_classes, seed = seed)
h2o.gbm.1 <- function(..., ntrees = 100, seed = 1) h2o.gbm.wrapper(..., ntrees = ntrees, seed = seed)
h2o.gbm.2 <- function(..., ntrees = 100, nbins = 50, seed = 1) h2o.gbm.wrapper(..., ntrees = ntrees, nbins = nbins, seed = seed)
h2o.gbm.3 <- function(..., ntrees = 100, max_depth = 10, seed = 1) h2o.gbm.wrapper(..., ntrees = ntrees, max_depth = max_depth, seed = seed)
h2o.gbm.4 <- function(..., ntrees = 100, col_sample_rate = 0.8, seed = 1) h2o.gbm.wrapper(..., ntrees = ntrees, col_sample_rate = col_sample_rate, seed = seed)
h2o.gbm.5 <- function(..., ntrees = 100, col_sample_rate = 0.7, seed = 1) h2o.gbm.wrapper(..., ntrees = ntrees, col_sample_rate = col_sample_rate, seed = seed)
h2o.gbm.6 <- function(..., ntrees = 100, col_sample_rate = 0.6, seed = 1) h2o.gbm.wrapper(..., ntrees = ntrees, col_sample_rate = col_sample_rate, seed = seed)
h2o.gbm.7 <- function(..., ntrees = 100, balance_classes = TRUE, seed = 1) h2o.gbm.wrapper(..., ntrees = ntrees, balance_classes = balance_classes, seed = seed)
h2o.gbm.8 <- function(..., ntrees = 100, max_depth = 3, seed = 1) h2o.gbm.wrapper(..., ntrees = ntrees, max_depth = max_depth, seed = seed)
h2o.deeplearning.1 <- function(..., hidden = c(500,500), activation = "Rectifier", epochs = 50, seed = 1)  h2o.deeplearning.wrapper(..., hidden = hidden, activation = activation, seed = seed)
h2o.deeplearning.2 <- function(..., hidden = c(200,200,200), activation = "Tanh", epochs = 50, seed = 1)  h2o.deeplearning.wrapper(..., hidden = hidden, activation = activation, seed = seed)
h2o.deeplearning.3 <- function(..., hidden = c(500,500), activation = "RectifierWithDropout", epochs = 50, seed = 1)  h2o.deeplearning.wrapper(..., hidden = hidden, activation = activation, seed = seed)
h2o.deeplearning.4 <- function(..., hidden = c(500,500), activation = "Rectifier", epochs = 50, balance_classes = TRUE, seed = 1)  h2o.deeplearning.wrapper(..., hidden = hidden, activation = activation, balance_classes = balance_classes, seed = seed)
h2o.deeplearning.5 <- function(..., hidden = c(100,100,100), activation = "Rectifier", epochs = 50, seed = 1)  h2o.deeplearning.wrapper(..., hidden = hidden, activation = activation, seed = seed)
h2o.deeplearning.6 <- function(..., hidden = c(50,50), activation = "Rectifier", epochs = 50, seed = 1)  h2o.deeplearning.wrapper(..., hidden = hidden, activation = activation, seed = seed)
h2o.deeplearning.7 <- function(..., hidden = c(100,100), activation = "Rectifier", epochs = 50, seed = 1)  h2o.deeplearning.wrapper(..., hidden = hidden, activation = activation, seed = seed)


# Set up ensemble parameters
family <- "gaussian"
# learner <- c("h2o.glm.wrapper", "h2o.glm.3",
#              "h2o.randomForest.1", "h2o.randomForest.2", "h2o.randomForest.3", "h2o.randomForest.4",
#              "h2o.gbm.1", "h2o.gbm.6", "h2o.gbm.8",
#              "h2o.deeplearning.1", "h2o.deeplearning.6", "h2o.deeplearning.7")
metalearner <- "h2o.glm.wrapper"
learner <- c("h2o.glm.wrapper",
             "h2o.randomForest.1", "h2o.randomForest.2",
             "h2o.gbm.1", "h2o.gbm.6", "h2o.gbm.8",
             "h2o.deeplearning.1", "h2o.deeplearning.6", "h2o.deeplearning.7") ## works best

# make x and y
y <- "SalePrice"
x <- setdiff(names(df.train), y)
# Build ensemble
fit <- h2o.ensemble(x = x, y = y, 
                    training_frame = df.train, 
                    family = family, 
                    learner = learner, 
                    metalearner = metalearner,
                    cvControl = list(V = 20))

# Test model performance
perf <- h2o.ensemble_performance(fit, newdata = df.test)
perf
# 1054726535.71763
# 1040000670.03277
# 681923624.14596
# 843209059.950096
# 596947553.951224
# 684214383.257697

# impute, centre and scale
# impute, log, centre and scale

# Make predictions
pred <- predict(fit, newdata = df.test)

# Get predictions from h2o
predictions <- as.data.frame(pred$pred)  
obs <- as.data.frame(df.test[,y])[,1]

# Compare predictions to observations
cbind(predictions, obs)[1:20,]

RMSE(predictions, obs)


########################################################
## SCORES
########################################################
sub.hex <- as.h2o(ss)

pred <- predict(fit, newdata = sub.hex)

# Get predictions from h2o
predictions <- as.data.frame(pred$pred)  

submission <- data.frame(Id=sub$Id, SalePrice=predictions)
names(submission) <- c('Id', 'SalePrice')
write.csv(submission, file='Results_ensemble.csv', row.names = F)



h2o.shutdown()









