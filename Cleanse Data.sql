

-- CASTING MENU
-- 1. INTEGERS
-- ,case		when isnumeric([age]) = 1 then convert( integer, [age]) 
--		else null 
--		end as Age
--
-- 2. DECIMAL
-- 		  ,case		when isnumeric([age]) = 1 then convert( float, [age]) 
--					else null 
--					end as Age
-- 3. BOOLEAN
-- 		  ,case		when isnumeric([myFlag]) = 1 then convert( bit, [myFlag]) 
--					else null 
--					end as myFlag
USE [DT_KAGGLE_2016]
GO


select 
		top 1000
		  [date]  -- date  -- Harriet
		  ,[cust_id] -- string -- Harriet
		  ,[emp_index] -- string -- Harriet
		  ,[cust_country] -- string -- Harriet
		  ,[gender] -- string -- Harriet
		  ,case		when isnumeric([age]) = 1 then convert( integer, [age]) 
					else null 
					end as Age
		  ,[first_contract_date] -- date -- Harriet
		  ,[new_cust_index] -- bit -- Harriet
		  ,[tenure] -- numeric -- Harriet
		  ,[primary_cust_flag] -- bit -- Harriet
		  ,[Last_date_as_primary_cust] -- date -- Harriet
		  ,[Cust_type_start_mth]  -- int-- Cameron
		  ,[cust_relation_type_start_mth] -- string-- Cameron
		  ,[residence_flag] -- string-- Cameron
		  ,[Foreigner_flag] -- string-- Cameron
		  ,[spouse_index] -- ??-- Cameron
		  ,[joining_channel] -- string-- Cameron
		  ,[is_deceased] -- bit-- Cameron
		  ,[address_type] -- bit-- Cameron
		  ,[Province_code] -- int-- Cameron
		  ,[Province_name]-- string-- Cameron
		  ,[Activity_index] -- string-- Vivian
		  ,[Gross_hh_income] -- numeric-- Vivian
		  ,[segmentation] -- string-- Vivian
		  ,[Saving_Account] -- string-- Vivian
		  ,[Guarantees] -- bit-- Vivian
		  ,[Current_Accounts] -- bit-- Vivian
		  ,[Derivada_Account] -- bit-- Vivian
		  ,[Payroll_Account]-- bit-- Vivian
		  ,[Junior_Account]-- bit-- Vivian
		  ,[Mas_particular_Account]-- bit-- Vivian
		  ,[particular_Account]-- bit-- Steph
		  ,[particular_Plus_Account]-- bit-- Steph
		  ,[Short_term_deposits]-- bit-- Steph
		  ,[Medium_term_deposits]-- bit-- Steph
		  ,[Long_term_deposits]-- bit-- Steph
		  ,[e_account]-- bit-- Steph
		  ,[Funds]-- bit-- Steph
		  ,[Mortgage]-- bit-- Steph
		  ,[Pensions]-- bit-- Steph
		  ,[Loans]-- bit-- Steph
		  ,case		when isnumeric(Taxes) = 1 then convert( bit, Taxes) 
					else null 
					end as Taxes
		  ,case when isnumeric(Credit_Card) = 1 then convert(bit, Credit_Card) 
					else null 
					end as Credit_Card
		  ,case when isnumeric(Securities) = 1 then convert(bit, Securities) 
					else null 
					end as Securities
		  ,case when isnumeric(Home_Account) = 1 then convert(bit, Home_Account) 
					else null 
					end as Home_Account
		  ,case when isnumeric(Payroll) = 1 then convert(bit, Payroll) 
					else null 
					end as Payroll
		  ,case when isnumeric([Pensions(2)]) = 1 then convert(bit, [Pensions(2)]) 
					else null 
					end as [Pensions(2)]
		  ,case when isnumeric([Direct Debit]) = 1 then convert(bit, [Direct Debit]) 
					else null 
					end as [Direct Debit]
	from [RAW_TRAIN_5PER_V2];


	--select top 10 * from [RAW_TRAIN_5PER]